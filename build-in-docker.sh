#!/bin/bash

MAINTEXFILE='math-for-robotics.tex'

BUILDDIRNAME='build'

BUILDSCRIPTPATH_IN_DOCKER='./build-in-docker.sh'
MOUNTPOINT_IN_DOCKER='/mnt'
WORKDIR_IN_DOCKER=${MOUNTPOINT_IN_DOCKER}

DOCKER_IMAGE="aergus/latex"

if [ -f /.dockerenv ]; then
    echo '---=== CONTAINERED ===---'
    if [ -x ./prereq-actions.sh ]; then
        ./prereq-actions.sh
    fi

    echo "Run PDF compilation"
    latexmk -shell-escape -pdf ${MAINTEXFILE}
    exit 0
fi

echo '---=== STILL ===---'

CURDIR=`pwd`
BUILDDIRPATH=${CURDIR}/${BUILDDIRNAME}

if [ "${1}" == "" ]; then
    check_docker_image=`docker images ${DOCKER_IMAGE} | wc -l`
    if [ "${check_docker_image}" != "2" ]; then
        echo "No ${DOCKER_IMAGE} image. Run 'sudo docker pull ${DOCKER_IMAGE}' first"
        exit 1
    fi

    if [ ! -d ${BUILDDIRPATH} ]; then
        echo "Create build directory"
        mkdir ${BUILDDIRPATH}
    fi
    echo "Update content of build directory"
    rsync --exclude "/${BUILDDIRNAME}" --exclude '/.*' -av ./ ${BUILDDIRNAME}/

    echo "Run docker container"
    docker run --rm -it -v ${BUILDDIRPATH}/:${MOUNTPOINT_IN_DOCKER} -w ${WORKDIR_IN_DOCKER} ${DOCKER_IMAGE} ${BUILDSCRIPTPATH_IN_DOCKER}
elif [ "${1}" == "clean" ]; then
    if [ -d ${BUILDDIRPATH} ]; then
        echo "Remove build directory"
        rm -rf ${BUILDDIRPATH}
    fi
else
    echo "Incorrect usage"
    exit 1
fi
